$(document).ready(function() {
    $.ajax({
        method: "GET",
        url: "https://api.myjson.com/bins/be53e",
        // POST, PUT, DELETE body data
        /* data: {
            text: "hanh"
        },*/
        success: function(res) {
            var html = "";

            for(var i = 0; i < res.data.length; i++) {
                html += '<div class="item ' + ((i % 3 == 2) ? 'last' : '') + '"><a href="./detail.html?key=' + res.data[i].key + '">'
                    + '<div class="image">'
                        + '<img src="' + res.data[i].image.url + '" alt="' + res.data[i].image.title + '" />'
                    + '</div>'
                    + '<div class="text">'
                        + '<p class="title">' + res.data[i].title + '</p>'
                        + '<p class="author">' + res.data[i].author
                            + '<span class="date">' + res.data[i].date + '</span>'
                        + '</p>'
                    + '</div>'
                + '</a></div>';
            }

            $('.content .middle').prepend(html);
            toastr.success('Get data successfully !!!', 'Success')
        },
        error: function(err) {
            toastr.error('Fail to get data !!!', 'Fail')
        }
    })
});